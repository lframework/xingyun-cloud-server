#!/bin/sh

export BUILD_ID=dontkillme
if [ -z $JAVA_HOME ] ;
then
echo "JAVA_HOME is not exist, please check"
exit 1
fi

export APP_HOME_BIN_PATH=$(cd `dirname $0`; pwd)
export APP_HOME=${APP_HOME_BIN_PATH%/bin}

STORAGE_TYPE=mysql MYSQL_USER=root MYSQL_PASS=password MYSQL_HOST=127.0.0.1 MYSQL_TCP_PORT=3306 $JAVA_HOME/bin/java -jar -server $APP_HOME/zipkin-server.jar >/dev/null 2>&1 &